<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>Installer</name>
    <message>
        <location filename="../../QtGUIInstaller/Installer.cpp" line="65"/>
        <source>FileWave Kiosk</source>
        <translation>FileWave Kiosk</translation>
    </message>
</context>
<context>
    <name>InstallerStatusDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="20"/>
        <source>FileWave Installer</source>
        <translation>FileWave 安装程序</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="35"/>
        <source>Important system software is now being installed on your computer...</source>
        <translation>正在您的计算机上安装重要的系统软件...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="51"/>
        <source>Do not shut down your computer.  It will automatically reboot when the installation is complete.</source>
        <translation>不要关闭您的计算机。  安装完成后，它会自动重新启动。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="70"/>
        <source>Do not use the start menu.</source>
        <translation>不要使用开始菜单。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="86"/>
        <source>Activating Fileset(s)...</source>
        <translation>正在激活文件集...</translation>
    </message>
</context>
<context>
    <name>KioskClient</name>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="254"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="255"/>
        <source>An Administrator requested remote access to this computer. A browser window will pop up.</source>
        <translation>将弹出一个浏览器窗口。</translation>
    </message>
</context>
<context>
    <name>QuitRunningProcessDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="14"/>
        <source>Quit All Running Processes</source>
        <translation>退出所有正在运行的进程</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="26"/>
        <source>Important System Software is to be installed on this computer...</source>
        <translation>要在此计算机上安装的重要系统软件...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="39"/>
        <source>Please save your work and exit all programs.</source>
        <translation>请保存您的工作并退出所有程序。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="52"/>
        <source>The following applications are preventing this installation from taking place.</source>
        <translation>以下应用程序阻止进行此安装。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="113"/>
        <source>Quit Application</source>
        <translation>退出应用程序</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="126"/>
        <source>Quit All</source>
        <translation>全部退出</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="149"/>
        <source>Delay</source>
        <translation>延迟</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="162"/>
        <source>for</source>
        <translation>时间：</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="198"/>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="384"/>
        <source>Start Installation</source>
        <translation>开始安装</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="213"/>
        <source>[Reboot deadline information...]</source>
        <translation>[重新启动截止日期信息...]</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="295"/>
        <source>%1 programs active</source>
        <translation>%1 个程序处于活动状态</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="292"/>
        <source>Installation will begin automatically in %1 seconds...</source>
        <translation>安装将在 %1 秒内自动开始...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="228"/>
        <source>You can delay the installation.</source>
        <translation>您可以延迟安装。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="232"/>
        <source>You can delay the installation until %1</source>
        <translation>您可以将安装延迟到 %1 之前</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="239"/>
        <source>This update is now mandatory and cannot be delayed.</source>
        <translation>此更新目前是强制性的，无法延迟.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="271"/>
        <source>15 Minutes</source>
        <translation>15 分钟</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="272"/>
        <source>30 Minutes</source>
        <translation>30 分钟</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="273"/>
        <source>1 Hour</source>
        <translation>1 小时</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="274"/>
        <source>2 Hours</source>
        <translation>2 小时</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="282"/>
        <source>deadline</source>
        <translation>截止日期</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="283"/>
        <source>till</source>
        <translation>直至</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="385"/>
        <source>At least one applications is preventing this installation from taking place.
Do you want to quit all applications before starting the installation?</source>
        <translation>至少一个应用程序正在阻止此安装程序的运行。
开始安装程序前，是否确实要退出所有安装程序?</translation>
    </message>
</context>
</TS>
