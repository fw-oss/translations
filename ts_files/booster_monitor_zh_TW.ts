<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW" sourcelanguage="en">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="29"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="30"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="31"/>
        <source>Booster Monitor</source>
        <translation>Booster Monitor</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="39"/>
        <source>File</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="40"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="40"/>
        <source>Ctrl+O</source>
        <comment>shortcut to open</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="42"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="42"/>
        <source>Ctrl+W</source>
        <comment>shortcut to close window</comment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="44"/>
        <source>About %1</source>
        <translation>關於 %1</translation>
    </message>
</context>
</TS>
