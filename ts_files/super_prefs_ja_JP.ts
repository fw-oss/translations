<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="23"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="24"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="25"/>
        <source>SuperPrefs Editor</source>
        <translation>SuperPrefsエディター</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="33"/>
        <source>File</source>
        <translation>ファイル</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="34"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="34"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="36"/>
        <source>About %1</source>
        <translation>%1 について</translation>
    </message>
</context>
</TS>
