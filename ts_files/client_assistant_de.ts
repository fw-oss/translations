<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>AssistantCustomDataDlg</name>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="14"/>
        <source>Custom Data Fields</source>
        <translation>Benutzerdefinierte Datenfelder</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="28"/>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="35"/>
        <source>Location</source>
        <translation>Standort</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="42"/>
        <source>Building</source>
        <translation>Gebäude</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="49"/>
        <source>Monitor ID</source>
        <translation>Überwachungs-ID</translation>
    </message>
</context>
<context>
    <name>AssistantDlg</name>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="14"/>
        <source>FileWave Client Installer Assistant</source>
        <translation>Installationsassistent des FileWave-Clients</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="37"/>
        <source>Welcome to the FileWave Client Setup Assistant</source>
        <translation>Willkommen beim Installationsassistenten des FileWave-Clients</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="47"/>
        <source>Please enter the initial preferences for this Client</source>
        <translation>Bitte geben Sie die Grundeinstellungen für diesen Client ein</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="70"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="77"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="90"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="129"/>
        <source>Booster:</source>
        <translation>Booster:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="212"/>
        <source>Use Computer Name for Client Name</source>
        <translation>Computername als Client-Name verwenden</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="257"/>
        <source>Client Name:</source>
        <translation>Client-Name:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="280"/>
        <source>Client Password:</source>
        <translation>Client-Passwort:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="306"/>
        <source>Confirm Password:</source>
        <translation>Bestätigen Sie das Passwort:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="358"/>
        <source>Edit Custom Data...</source>
        <translation>Benutzerdefinierte Daten bearbeiten...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.cpp" line="227"/>
        <source>The password and confirmation don&apos;t match -- please try again.</source>
        <translation>Passwort und Bestätigung stimmen nicht überein - bitte versuchen Sie es erneut.</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="22"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="23"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="24"/>
        <source>FileWave Client Assistant</source>
        <translation>Assistent des FileWave-Clients</translation>
    </message>
</context>
</TS>
