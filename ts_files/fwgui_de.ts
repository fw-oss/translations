<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>Installer</name>
    <message>
        <location filename="../../QtGUIInstaller/Installer.cpp" line="65"/>
        <source>FileWave Kiosk</source>
        <translation>FileWave Kiosk</translation>
    </message>
</context>
<context>
    <name>InstallerStatusDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="20"/>
        <source>FileWave Installer</source>
        <translation>FileWave Installer</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="35"/>
        <source>Important system software is now being installed on your computer...</source>
        <translation>Wichtige Systemsoftware wird jetzt auf Ihrem Computer installiert...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="51"/>
        <source>Do not shut down your computer.  It will automatically reboot when the installation is complete.</source>
        <translation>Schalten Sie ihren Computer nicht aus. Er wird nach Abschluss der Installation automatisch neu gestartet.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="70"/>
        <source>Do not use the start menu.</source>
        <translation>Verwenden Sie bitte das Start Menü zur Zeit nicht.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="86"/>
        <source>Activating Fileset(s)...</source>
        <translation>Fileset(s) wird/werden aktiviert...</translation>
    </message>
</context>
<context>
    <name>KioskClient</name>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="254"/>
        <source>Information</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="255"/>
        <source>An Administrator requested remote access to this computer. A browser window will pop up.</source>
        <translation>Ein Browserfenster wird eingeblendet.</translation>
    </message>
</context>
<context>
    <name>QuitRunningProcessDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="14"/>
        <source>Quit All Running Processes</source>
        <translation>Alle laufenden Programme beenden</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="26"/>
        <source>Important System Software is to be installed on this computer...</source>
        <translation>Wichtige System-Updates sollen auf ihrem Computer installiert werden...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="39"/>
        <source>Please save your work and exit all programs.</source>
        <translation>Bitte speichern Sie alle offenen Dokumente ab und beenden Sie alle Programme.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="52"/>
        <source>The following applications are preventing this installation from taking place.</source>
        <translation>Die folgenden Anwendungen behindern den Anfang der Installation.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="113"/>
        <source>Quit Application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="126"/>
        <source>Quit All</source>
        <translation>Alle Programme beenden</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="149"/>
        <source>Delay</source>
        <translation>Verzögern</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="162"/>
        <source>for</source>
        <translation>bei</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="213"/>
        <source>[Reboot deadline information...]</source>
        <translation>[Reboot deadline information...]</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="198"/>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="384"/>
        <source>Start Installation</source>
        <translation>Installation beginnen</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="239"/>
        <source>This update is now mandatory and cannot be delayed.</source>
        <translation>Diese Aktualisierung ist jetzt unbedingt notwendig und kann nicht verschoben werden.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="271"/>
        <source>15 Minutes</source>
        <translation>15 min</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="272"/>
        <source>30 Minutes</source>
        <translation>30 min</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="273"/>
        <source>1 Hour</source>
        <translation>1 Std</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="274"/>
        <source>2 Hours</source>
        <translation>2 Std</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="282"/>
        <source>deadline</source>
        <translation>Frist</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="283"/>
        <source>till</source>
        <translation>bis</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="385"/>
        <source>At least one applications is preventing this installation from taking place.
Do you want to quit all applications before starting the installation?</source>
        <translation>Mindestens eine Anwendung verhindert, dass diese Installation durchgeführt wird.
Möchten Sie alle Anwendungen beenden, bevor Sie die Installation starten?</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="295"/>
        <source>%1 programs active</source>
        <translation>%1 Programme sind offen</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="228"/>
        <source>You can delay the installation.</source>
        <translation>Sie können die Installation verzögern.</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="232"/>
        <source>You can delay the installation until %1</source>
        <translation>Sie können die Installation bis %1 verzögern</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="292"/>
        <source>Installation will begin automatically in %1 seconds...</source>
        <translation>Die Installation wird automatisch in %1 Sekunden beginnen...</translation>
    </message>
</context>
</TS>
