<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="29"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="30"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="31"/>
        <source>Booster Monitor</source>
        <translation>Booster Monitor</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="39"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="40"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="40"/>
        <source>Ctrl+O</source>
        <comment>shortcut to open</comment>
        <translation>Strg+O</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="42"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="42"/>
        <source>Ctrl+W</source>
        <comment>shortcut to close window</comment>
        <translation>Strg+W</translation>
    </message>
    <message>
        <location filename="../../QtBoosterMonitor/main.cpp" line="44"/>
        <source>About %1</source>
        <translation>Zu %1</translation>
    </message>
</context>
</TS>
