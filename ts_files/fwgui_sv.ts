<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Installer</name>
    <message>
        <location filename="../../QtGUIInstaller/Installer.cpp" line="65"/>
        <source>FileWave Kiosk</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstallerStatusDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="20"/>
        <source>FileWave Installer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="35"/>
        <source>Important system software is now being installed on your computer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="51"/>
        <source>Do not shut down your computer.  It will automatically reboot when the installation is complete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="70"/>
        <source>Do not use the start menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="86"/>
        <source>Activating Fileset(s)...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KioskClient</name>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="254"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="255"/>
        <source>An Administrator requested remote access to this computer. A browser window will pop up.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuitRunningProcessDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="14"/>
        <source>Quit All Running Processes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="26"/>
        <source>Important System Software is to be installed on this computer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="39"/>
        <source>Please save your work and exit all programs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="52"/>
        <source>The following applications are preventing this installation from taking place.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="113"/>
        <source>Quit Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="126"/>
        <source>Quit All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="149"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="162"/>
        <source>for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="198"/>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="384"/>
        <source>Start Installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="213"/>
        <source>[Reboot deadline information...]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="228"/>
        <source>You can delay the installation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="232"/>
        <source>You can delay the installation until %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="239"/>
        <source>This update is now mandatory and cannot be delayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="271"/>
        <source>15 Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="272"/>
        <source>30 Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="273"/>
        <source>1 Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="274"/>
        <source>2 Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="282"/>
        <source>deadline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="283"/>
        <source>till</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="292"/>
        <source>Installation will begin automatically in %1 seconds...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="295"/>
        <source>%1 programs active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="385"/>
        <source>At least one applications is preventing this installation from taking place.
Do you want to quit all applications before starting the installation?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
