<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW" sourcelanguage="en">
<context>
    <name>AssistantCustomDataDlg</name>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="14"/>
        <source>Custom Data Fields</source>
        <translation>自訂資料欄位</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="28"/>
        <source>Department</source>
        <translation>部門</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="35"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="42"/>
        <source>Building</source>
        <translation>建築物</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/AssistantCustomDataDlg.ui" line="49"/>
        <source>Monitor ID</source>
        <translation>監視器 ID</translation>
    </message>
</context>
<context>
    <name>AssistantDlg</name>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="14"/>
        <source>FileWave Client Installer Assistant</source>
        <translation>FileWave 用戶端安裝程式小幫手</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="37"/>
        <source>Welcome to the FileWave Client Setup Assistant</source>
        <translation>歡迎使用 FileWave 用戶端設定小幫手</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="47"/>
        <source>Please enter the initial preferences for this Client</source>
        <translation>請為此用戶端輸入初始喜好設定</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="70"/>
        <source>Address:</source>
        <translation>位址:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="77"/>
        <source>Port:</source>
        <translation>連接埠:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="90"/>
        <source>Server:</source>
        <translation>伺服器:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="129"/>
        <source>Booster:</source>
        <translation>Booster:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="212"/>
        <source>Use Computer Name for Client Name</source>
        <translation>將電腦名稱用作用戶端名稱</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="257"/>
        <source>Client Name:</source>
        <translation>用戶端名稱:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="280"/>
        <source>Client Password:</source>
        <translation>用戶端密碼:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="306"/>
        <source>Confirm Password:</source>
        <translation>確認密碼:</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.ui" line="358"/>
        <source>Edit Custom Data...</source>
        <translation>編輯自訂資料...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtClientAssistant/assistantdlg.cpp" line="227"/>
        <source>The password and confirmation don&apos;t match -- please try again.</source>
        <translation>密碼與確認密碼不相符 -- 請重試。</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="22"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="23"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtClientAssistant/main.cpp" line="24"/>
        <source>FileWave Client Assistant</source>
        <translation>FileWave 用戶端小幫手</translation>
    </message>
</context>
</TS>
