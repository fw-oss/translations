<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW" sourcelanguage="en">
<context>
    <name>Installer</name>
    <message>
        <location filename="../../QtGUIInstaller/Installer.cpp" line="65"/>
        <source>FileWave Kiosk</source>
        <translation>FileWave Kiosk</translation>
    </message>
</context>
<context>
    <name>InstallerStatusDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="20"/>
        <source>FileWave Installer</source>
        <translation>FileWave 安裝程式</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="35"/>
        <source>Important system software is now being installed on your computer...</source>
        <translation>現在正在您的電腦中安裝重要系統軟體...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="51"/>
        <source>Do not shut down your computer.  It will automatically reboot when the installation is complete.</source>
        <translation>不要關閉您的電腦。  當安裝完成時，將自動重新啟動。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="70"/>
        <source>Do not use the start menu.</source>
        <translation>不要使用「開始」功能表。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="86"/>
        <source>Activating Fileset(s)...</source>
        <translation>正在啟用檔案集...</translation>
    </message>
</context>
<context>
    <name>KioskClient</name>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="254"/>
        <source>Information</source>
        <translation>資訊</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="255"/>
        <source>An Administrator requested remote access to this computer. A browser window will pop up.</source>
        <translation>將顯示出瀏覽器視窗。</translation>
    </message>
</context>
<context>
    <name>QuitRunningProcessDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="14"/>
        <source>Quit All Running Processes</source>
        <translation>結束所有正在執行的處理序</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="26"/>
        <source>Important System Software is to be installed on this computer...</source>
        <translation>將在此電腦上安裝重要系統軟體...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="39"/>
        <source>Please save your work and exit all programs.</source>
        <translation>請儲存您的工作並結束所有程式。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="52"/>
        <source>The following applications are preventing this installation from taking place.</source>
        <translation>下列應用程式正在阻止進行此安裝。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="113"/>
        <source>Quit Application</source>
        <translation>結束應用程式</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="126"/>
        <source>Quit All</source>
        <translation>結束全部</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="149"/>
        <source>Delay</source>
        <translation>延遲</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="162"/>
        <source>for</source>
        <translation>適用於</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="198"/>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="384"/>
        <source>Start Installation</source>
        <translation>開始安裝</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="213"/>
        <source>[Reboot deadline information...]</source>
        <translation>[重新啟動期限資訊...]</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="295"/>
        <source>%1 programs active</source>
        <translation>%1 個程式為使用中</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="292"/>
        <source>Installation will begin automatically in %1 seconds...</source>
        <translation>安裝將在 %1 秒後自動開始...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="228"/>
        <source>You can delay the installation.</source>
        <translation>您可延遲此安裝。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="232"/>
        <source>You can delay the installation until %1</source>
        <translation>您可將安裝延遲至 %1</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="239"/>
        <source>This update is now mandatory and cannot be delayed.</source>
        <translation>此更新現在必須進行，無法延遲。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="271"/>
        <source>15 Minutes</source>
        <translation>15 分鐘</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="272"/>
        <source>30 Minutes</source>
        <translation>30 分鐘</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="273"/>
        <source>1 Hour</source>
        <translation>1 小時</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="274"/>
        <source>2 Hours</source>
        <translation>2 小時</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="282"/>
        <source>deadline</source>
        <translation>期限</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="283"/>
        <source>till</source>
        <translation>至</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="385"/>
        <source>At least one applications is preventing this installation from taking place.
Do you want to quit all applications before starting the installation?</source>
        <translation>至少一個應用程式正在阻止進行此安裝。
是否想要開始安裝前結束所有應用程式?</translation>
    </message>
</context>
</TS>
