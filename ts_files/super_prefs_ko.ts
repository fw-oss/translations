<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR" sourcelanguage="en">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="23"/>
        <source>FileWave</source>
        <translation>FileWave</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="24"/>
        <source>filewave.com</source>
        <translation>filewave.com</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="25"/>
        <source>SuperPrefs Editor</source>
        <translation>SuperPrefs 편집기</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="33"/>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="34"/>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="34"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../../QtSuperPrefsEditor/main.cpp" line="36"/>
        <source>About %1</source>
        <translation>%1 정보</translation>
    </message>
</context>
</TS>
