<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en_US">
<context>
    <name>Installer</name>
    <message>
        <location filename="../../QtGUIInstaller/Installer.cpp" line="65"/>
        <source>FileWave Kiosk</source>
        <translation>FileWave Kiosk</translation>
    </message>
</context>
<context>
    <name>InstallerStatusDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="20"/>
        <source>FileWave Installer</source>
        <translation>FileWaveインストーラー</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="35"/>
        <source>Important system software is now being installed on your computer...</source>
        <translation>重要なシステムソフトウェアがコンピューターにインストールされています...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="51"/>
        <source>Do not shut down your computer.  It will automatically reboot when the installation is complete.</source>
        <translation>コンピューターをシャットダウンしないでください。  インストールが完了すると自動的に再起動します。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="70"/>
        <source>Do not use the start menu.</source>
        <translation>スタートメニューを使用しないでください。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/InstallerStatusDialog.ui" line="86"/>
        <source>Activating Fileset(s)...</source>
        <translation>ファイルセットをアクティベートします...</translation>
    </message>
</context>
<context>
    <name>KioskClient</name>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="254"/>
        <source>Information</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/KioskClient.cpp" line="255"/>
        <source>An Administrator requested remote access to this computer. A browser window will pop up.</source>
        <translation>ブラウザのウィンドウがポップアップ表示されます。</translation>
    </message>
</context>
<context>
    <name>QuitRunningProcessDialog</name>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="14"/>
        <source>Quit All Running Processes</source>
        <translation>すべての実行中プロセスを終了</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="26"/>
        <source>Important System Software is to be installed on this computer...</source>
        <translation>重要なシステムソフトウェアは、このコンピューターにインストールされます...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="39"/>
        <source>Please save your work and exit all programs.</source>
        <translation>作業を保存してすべてのプログラムを終了してください。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="52"/>
        <source>The following applications are preventing this installation from taking place.</source>
        <translation>以下のアプリがこのインストールの実行を妨げています。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="113"/>
        <source>Quit Application</source>
        <translation>アプリを終了</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="126"/>
        <source>Quit All</source>
        <translation>すべて終了</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="149"/>
        <source>Delay</source>
        <translation>遅らせる</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="162"/>
        <source>for</source>
        <translation>対象</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="198"/>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="384"/>
        <source>Start Installation</source>
        <translation>インストール開始</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.ui" line="213"/>
        <source>[Reboot deadline information...]</source>
        <translation>[リブート期限情報...]</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="295"/>
        <source>%1 programs active</source>
        <translation>%1 プログラムがアクティブ</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="292"/>
        <source>Installation will begin automatically in %1 seconds...</source>
        <translation>インストールはn %1秒後に自動的に始まります...</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="228"/>
        <source>You can delay the installation.</source>
        <translation>インストールを遅延させることができます。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="232"/>
        <source>You can delay the installation until %1</source>
        <translation>%1 までインストールを遅延させることができます</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="239"/>
        <source>This update is now mandatory and cannot be delayed.</source>
        <translation>この更新は現在必須であり、遅らせることはできません。</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="271"/>
        <source>15 Minutes</source>
        <translation>15 分</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="272"/>
        <source>30 Minutes</source>
        <translation>30 分</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="273"/>
        <source>1 Hour</source>
        <translation>1 時間</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="274"/>
        <source>2 Hours</source>
        <translation>2 時間</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="282"/>
        <source>deadline</source>
        <translation>期限</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="283"/>
        <source>till</source>
        <translation>まで</translation>
    </message>
    <message>
        <location filename="../../QtGUIInstaller/QuitRunningProcessDialog.cpp" line="385"/>
        <source>At least one applications is preventing this installation from taking place.
Do you want to quit all applications before starting the installation?</source>
        <translation>少なくとも1つのアプリがこのインストールの実行を妨げています。
インストールを開始する前にすべてのアプリを終了しますか?</translation>
    </message>
</context>
</TS>
