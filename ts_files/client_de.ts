<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>FW::Catalog</name>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="545"/>
        <source>Checking for new model version</source>
        <translation>Nach neuer Modellversion suchen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="567"/>
        <source>Check for new model in %1 seconds</source>
        <translation>Suche nach neuem Modell in %1 s</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="848"/>
        <source>Checking Files...</source>
        <translation>Dateien werden gesucht...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="849"/>
        <source>Verifying Files...</source>
        <translation>Dateien werden geprüft...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="868"/>
        <source>Verifying %1 of %2 filesets</source>
        <translation>%1 von %2 Filesets werden geprüft</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="934"/>
        <source>Running..</source>
        <translation>Läuft..</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="936"/>
        <source>Finished Verifying Files...</source>
        <translation>Überprüfung der Dateien abgeschlossen...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1595"/>
        <source>Downloading User Manifest</source>
        <translation>Benutzermanifest wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1622"/>
        <source>Downloading Imaging Manifest</source>
        <translation>Imaging-Manifest wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1649"/>
        <source>Downloading Smart Filter Manifests</source>
        <translation>Intelligentes Filtermanifest wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1688"/>
        <location filename="../../QtClient/Catalog.cpp" line="1706"/>
        <location filename="../../QtClient/Catalog.cpp" line="2179"/>
        <source>Running</source>
        <translation>Läuft</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4092"/>
        <source>Waiting for Booster - %1</source>
        <translation>Booster ausstehend - %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4111"/>
        <source>%1 (or dependency of) not found on server</source>
        <translation>%1 (oder entsprechende Abhängigkeit) nicht auf dem Server gefunden</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4466"/>
        <source>Downloading Fileset Container (%1)</source>
        <translation>Filesetcontainer wird geladen (%1)</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4468"/>
        <source>Processing requirements of %1</source>
        <translation>Voraussetzungen von %1 werden verarbeitet</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4577"/>
        <source>No space left on device...Cannot download new Filesets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4589"/>
        <source>Downloading fileset %1 of %2</source>
        <translation>Fileset %1 von %2 wird geladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4628"/>
        <source>Activating %1</source>
        <translation>%1 wird aktiviert</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4638"/>
        <source>Activating fileset %1 of %2</source>
        <translation>Fileset %1 von %2 wird aktiviert</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4753"/>
        <source>Installing Software...</source>
        <translation>Software wird installiert...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4889"/>
        <source> Installation Failed</source>
        <translation> Installation fehlerhaft</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4896"/>
        <source> Installation Successfully</source>
        <translation> Installation abgeschlossen</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5252"/>
        <source>Updating %1 to new version</source>
        <translation>%1 wird auf neue Version aktualisiert</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5839"/>
        <source>Current Model Number: %1

Filesets in Inventory:

</source>
        <translation>Aktuelle Modellnummer %1 

Filesets im Inventar:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5853"/>
        <source>Filesets not meeting requirements:

</source>
        <translation>Filesets erfüllen nicht die Anforderungen:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5866"/>
        <source>
Worklist:

</source>
        <translation>
Arbeitsliste:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5876"/>
        <source>&lt;Fileset name still unknown&gt; ID:%1, revision ID:%2</source>
        <translation>&lt;Fileset name still unknown&gt; ID:%1, Revisions-ID:%2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5951"/>
        <source>This computer will be shut down once its data is erased</source>
        <translation>Dieser Computer wird heruntergefahren, sobald seine Daten gelöscht sind</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5952"/>
        <source>Executing Remote Wipe</source>
        <translation>Fernlöschung wird ausgeführt</translation>
    </message>
</context>
<context>
    <name>FW::FilesetContainer</name>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="563"/>
        <source>Fileset </source>
        <translation>Fileset </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="575"/>
        <source>revision </source>
        <translation>Revision </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="608"/>
        <source>Processing %1</source>
        <translation>%1 wird verarbeitet</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="907"/>
        <source>Sending download request to server...</source>
        <translation type="unfinished">Download-Anfrage wird an den Server gesendet...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="913"/>
        <source>Downloading %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1621"/>
        <source>Deleting empty folders of %1</source>
        <translation>Leere Ordner von %1 werden gelöscht</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1672"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1796"/>
        <source>Making %1 passive</source>
        <translation>%1 wird passiviert</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1886"/>
        <source>Deleting %1</source>
        <translation>%1 wird/werden gelöscht</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1952"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2015"/>
        <source>Executing %1...</source>
        <translation>%1 wird/werden ausgeführt...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1954"/>
        <source>Executing Files...</source>
        <translation>Dateien werden ausgeführt...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2094"/>
        <source>Checking Files..</source>
        <translation>Dateien werden gesucht..</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2137"/>
        <source>Activating Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2219"/>
        <source>Running..</source>
        <translation>Läuft..</translation>
    </message>
</context>
<context>
    <name>FW::MainController</name>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="208"/>
        <source>Client is archived.</source>
        <translation>Client wird archiviert.</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="213"/>
        <source>Running...</source>
        <translation>Läuft...</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="219"/>
        <source>Installing %1</source>
        <translation>%1 Wird installiert</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="224"/>
        <location filename="../../QtClient/MainController.cpp" line="344"/>
        <source>Blocked by local restriction</source>
        <translation>Durch lokale Einschränkung blockiert</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="367"/>
        <source>Connecting to FW Server...</source>
        <translation>Verbindung zum FW-Server wird aufgebaut...</translation>
    </message>
</context>
<context>
    <name>FW::ServerConnection</name>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1242"/>
        <source>Building list of files to download...</source>
        <translation>Liste der herunterzuladenden Dateien wird aufgebaut...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1261"/>
        <source>Downloading subset for %1</source>
        <translation>Teilmenge für %1 wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1320"/>
        <location filename="../../QtClient/ServerConnection.cpp" line="1556"/>
        <source>Downloading %1</source>
        <translation>%1 wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1474"/>
        <source>Sending download request to server...</source>
        <translation>Download-Anfrage wird an den Server gesendet...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1557"/>
        <source>Downloading...</source>
        <translation>Wird heruntergeladen...</translation>
    </message>
</context>
<context>
    <name>KioskServerController</name>
    <message>
        <location filename="../../QtClient/kiosk/KioskServerController.cpp" line="377"/>
        <source>Installing Software...</source>
        <translation>Software wird installiert...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="97"/>
        <source>Fileset </source>
        <translation>Fileset </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="107"/>
        <source>revision </source>
        <translation>Revision </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="127"/>
        <source>checkRequirements</source>
        <comment>ActionRecord</comment>
        <translation>checkRequirements</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="128"/>
        <source>downloadFiles</source>
        <comment>ActionRecord</comment>
        <translation>downloadFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="129"/>
        <source>activateFiles</source>
        <comment>ActionRecord</comment>
        <translation>activateFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="130"/>
        <source>makeFilesPassive</source>
        <comment>ActionRecord</comment>
        <translation>makeFilesPassive</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="131"/>
        <source>updateFileset</source>
        <comment>ActionRecord</comment>
        <translation>updateFileset</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="132"/>
        <source>deleteFiles</source>
        <comment>ActionRecord</comment>
        <translation>deleteFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="133"/>
        <source>updateFilesetDependencies</source>
        <comment>ActionRecord</comment>
        <translation>updateFilesetDependencies</translation>
    </message>
</context>
</TS>
