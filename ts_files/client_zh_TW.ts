<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW" sourcelanguage="en">
<context>
    <name>FW::Catalog</name>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="545"/>
        <source>Checking for new model version</source>
        <translation>正在檢查新模型版本</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="567"/>
        <source>Check for new model in %1 seconds</source>
        <translation>將在 %1 秒後檢查新模型</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="848"/>
        <source>Checking Files...</source>
        <translation>正在檢查檔案...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="849"/>
        <source>Verifying Files...</source>
        <translation>正在驗證檔案...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="868"/>
        <source>Verifying %1 of %2 filesets</source>
        <translation>正在驗證第 %1 個檔案集，共 %2 個</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="934"/>
        <source>Running..</source>
        <translation>正在執行..</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="936"/>
        <source>Finished Verifying Files...</source>
        <translation>驗證檔案已完成...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1595"/>
        <source>Downloading User Manifest</source>
        <translation>正在下載使用者資訊清單</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1622"/>
        <source>Downloading Imaging Manifest</source>
        <translation>正在下載映像資訊清單</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1649"/>
        <source>Downloading Smart Filter Manifests</source>
        <translation>正在下載智慧型篩選器資訊清單</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1688"/>
        <location filename="../../QtClient/Catalog.cpp" line="1706"/>
        <location filename="../../QtClient/Catalog.cpp" line="2179"/>
        <source>Running</source>
        <translation>正在執行</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4092"/>
        <source>Waiting for Booster - %1</source>
        <translation>正在等待 Booster - %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4111"/>
        <source>%1 (or dependency of) not found on server</source>
        <translation>在伺服器上找不到 %1（或其相依性）</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4466"/>
        <source>Downloading Fileset Container (%1)</source>
        <translation>正在下載檔案集容器 (%1)</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4468"/>
        <source>Processing requirements of %1</source>
        <translation>正在處理 %1 的要求</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4577"/>
        <source>No space left on device...Cannot download new Filesets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4589"/>
        <source>Downloading fileset %1 of %2</source>
        <translation>正在下載檔案集 %1，共 %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4628"/>
        <source>Activating %1</source>
        <translation>正在啟用 %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4638"/>
        <source>Activating fileset %1 of %2</source>
        <translation>正在啟用檔案集 %1，共 %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4753"/>
        <source>Installing Software...</source>
        <translation>正在安裝軟體...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4889"/>
        <source> Installation Failed</source>
        <translation>安裝失敗</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4896"/>
        <source> Installation Successfully</source>
        <translation>安裝成功</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5252"/>
        <source>Updating %1 to new version</source>
        <translation>正在將 %1 更新至新版本</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5839"/>
        <source>Current Model Number: %1

Filesets in Inventory:

</source>
        <translation>目前型號：%1

庫存中的檔案集:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5853"/>
        <source>Filesets not meeting requirements:

</source>
        <translation>不滿足要求的檔案集:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5866"/>
        <source>
Worklist:

</source>
        <translation>
工作清單:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5876"/>
        <source>&lt;Fileset name still unknown&gt; ID:%1, revision ID:%2</source>
        <translation>&lt;檔案集名稱仍未知&gt; ID：%1，修訂版本 ID：%2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5951"/>
        <source>This computer will be shut down once its data is erased</source>
        <translation>在清除其資料後，本電腦將關機</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5952"/>
        <source>Executing Remote Wipe</source>
        <translation>正在執行遠端抹除</translation>
    </message>
</context>
<context>
    <name>FW::FilesetContainer</name>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="563"/>
        <source>Fileset </source>
        <translation>檔案集 </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="575"/>
        <source>revision </source>
        <translation>修訂版本 </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="608"/>
        <source>Processing %1</source>
        <translation>正在處理 %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="907"/>
        <source>Sending download request to server...</source>
        <translation type="unfinished">正在傳送下載要求至伺服器...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="913"/>
        <source>Downloading %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1621"/>
        <source>Deleting empty folders of %1</source>
        <translation>正在刪除 %1 的空資料夾</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1672"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1796"/>
        <source>Making %1 passive</source>
        <translation>正在將 %1 設為被動</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1886"/>
        <source>Deleting %1</source>
        <translation>正在刪除 %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1952"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2015"/>
        <source>Executing %1...</source>
        <translation>正在執行 %1...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1954"/>
        <source>Executing Files...</source>
        <translation>正在執行檔案...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2094"/>
        <source>Checking Files..</source>
        <translation>正在檢查檔案..</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2137"/>
        <source>Activating Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2219"/>
        <source>Running..</source>
        <translation>正在執行..</translation>
    </message>
</context>
<context>
    <name>FW::MainController</name>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="208"/>
        <source>Client is archived.</source>
        <translation>用戶端已封存。</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="213"/>
        <source>Running...</source>
        <translation>正在執行...</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="219"/>
        <source>Installing %1</source>
        <translation>正在安裝 %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="224"/>
        <location filename="../../QtClient/MainController.cpp" line="344"/>
        <source>Blocked by local restriction</source>
        <translation>已被本機限制封鎖</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="367"/>
        <source>Connecting to FW Server...</source>
        <translation>正在連線至 FW 伺服器...</translation>
    </message>
</context>
<context>
    <name>FW::ServerConnection</name>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1242"/>
        <source>Building list of files to download...</source>
        <translation>正在建置要下載的檔案清單...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1261"/>
        <source>Downloading subset for %1</source>
        <translation>正在下載 %1 的子集</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1320"/>
        <location filename="../../QtClient/ServerConnection.cpp" line="1556"/>
        <source>Downloading %1</source>
        <translation>正在下載 %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1474"/>
        <source>Sending download request to server...</source>
        <translation>正在傳送下載要求至伺服器...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1557"/>
        <source>Downloading...</source>
        <translation>正在下載...</translation>
    </message>
</context>
<context>
    <name>KioskServerController</name>
    <message>
        <location filename="../../QtClient/kiosk/KioskServerController.cpp" line="377"/>
        <source>Installing Software...</source>
        <translation>正在安裝軟體...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="97"/>
        <source>Fileset </source>
        <translation>檔案集 </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="107"/>
        <source>revision </source>
        <translation>修訂版本 </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="127"/>
        <source>checkRequirements</source>
        <comment>ActionRecord</comment>
        <translation>checkRequirements</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="128"/>
        <source>downloadFiles</source>
        <comment>ActionRecord</comment>
        <translation>downloadFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="129"/>
        <source>activateFiles</source>
        <comment>ActionRecord</comment>
        <translation>activateFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="130"/>
        <source>makeFilesPassive</source>
        <comment>ActionRecord</comment>
        <translation>makeFilesPassive</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="131"/>
        <source>updateFileset</source>
        <comment>ActionRecord</comment>
        <translation>updateFileset</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="132"/>
        <source>deleteFiles</source>
        <comment>ActionRecord</comment>
        <translation>deleteFiles</translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="133"/>
        <source>updateFilesetDependencies</source>
        <comment>ActionRecord</comment>
        <translation>updateFilesetDependencies</translation>
    </message>
</context>
</TS>
