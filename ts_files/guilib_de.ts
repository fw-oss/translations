<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="94"/>
        <source>Version  %1</source>
        <translation>Version  %1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="109"/>
        <source>US Patent # 7,904,900 B2</source>
        <translation>US Patent # 7,904,900 B2</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="124"/>
        <source>Copyright © FileWave AG %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoosterPreferencesDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="14"/>
        <source>FileWave Booster Preferences</source>
        <translation>Booster-Einstellungen FileWave</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="20"/>
        <source>Booster Prefs</source>
        <translation>Booster-Einstellungen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="26"/>
        <source>Booster Name:</source>
        <translation>Name des Boosters:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="36"/>
        <source>Booster Location:</source>
        <translation>Booster-Ort:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="52"/>
        <source>Booster Port:</source>
        <translation>Booster-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="87"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="110"/>
        <source>Confirmation:</source>
        <translation>Bestätigung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="127"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave-Serveradresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="140"/>
        <source>Inventory port:</source>
        <translation>Inventar-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="183"/>
        <source>Number of Threads:</source>
        <translation>Anzahl der Threads:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="194"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="199"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="204"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="209"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="214"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="219"/>
        <source>96</source>
        <translation>96</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="224"/>
        <source>128</source>
        <translation>128</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="232"/>
        <source>Maximum Client Connections:</source>
        <translation>Maximale Client-Verbindungen:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="269"/>
        <source>Debug Level:</source>
        <translation>Debug-Level:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="280"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="285"/>
        <source>99</source>
        <translation>99</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="290"/>
        <source>101</source>
        <translation>101</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="304"/>
        <source>Delete Unused Filesets:</source>
        <translation>Nicht verwendete Filesets löschen:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="330"/>
        <source>Fileset Validation Interval:</source>
        <translation>Intervall für die Filesetüberprüfung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="364"/>
        <source>hours</source>
        <translation>Stunden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="379"/>
        <source>Client Download Speed Limit:</source>
        <translation>Geschwindigkeitslimit des Client-Downloads:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="413"/>
        <source>Each client will be limited to the specified download rate from this booster.  Uncheck this box to remove the download limit.</source>
        <translation>Jeder Client wird von diesem Booster auf die angegebene Downloadrate begrenzt.  Deaktivieren Sie dieses Kontrollkästchen, wird das Download-Limit aufgehoben.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="432"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="444"/>
        <source>Booster Server Prefs</source>
        <translation>Booster-Servereinstellungen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="585"/>
        <source>Server 5:</source>
        <translation>Server 5:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="598"/>
        <source>Server 2:</source>
        <translation>Server 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="542"/>
        <source>IP or DNS Address</source>
        <translation>IP- oder DNS-Adresse</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="568"/>
        <source>Server 4:</source>
        <translation>Server 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="522"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="532"/>
        <source>Server 3:</source>
        <translation>Server 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="558"/>
        <source>Server 1:</source>
        <translation>Server 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="63"/>
        <source>Booster Error</source>
        <translation>Booster-Fehler</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="64"/>
        <source>Failed to get a valid connection to Booster %1:%2</source>
        <translation>Es konnte keine korrekte Verbindung zu Booster %1:%2 aufgebaut werden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="121"/>
        <source>Password verification failed</source>
        <translation>Passwortüberprüfung fehlerhaft</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="127"/>
        <source>FileWave Server Address cannot be empty</source>
        <translation>FileWave-Serveradresse darf nicht leer sein</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="145"/>
        <source>&quot;%1&quot; for field %2 is an invalid value and was adjusted. Please check.</source>
        <translation>&quot;%1&quot; in Feld %2 ist ein ungültiger Wert und wurde geändert. Bitte kontrollieren.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="183"/>
        <source>Illegal Port Used: 20016</source>
        <translation>Ungültiger Port verwendet: 20016</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="184"/>
        <source>Use port 20015 to connect to the FileWave Server or 20013 to connect to other Boosters. Port 20016 is reserved for the FileWave Admin</source>
        <translation>Für die Verbindung mit dem FileWave-Server verwenden Sie Port 20015, für die Verbindung mit anderen Boostern 20013. Port 20016 ist dem FileWave Admin vorbehalten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="211"/>
        <source>Non-standard server ports were chosen.</source>
        <translation>Nicht standardisierte Server-Port gewählt.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="212"/>
        <source>Use port 20015 or 20013 to connect to the FileWave Server or other Boosters. Do you want to save anyway?

You may ignore this warning if you have a customized setup. </source>
        <translation>Für die Verbindung mit dem FileWave-Server oder anderen Boostern verwenden Sie Port 20015 oder 20013. Möchten Sie trotzdem speichern?

Sie können diese Warnung ignorieren, wenn Sie eine angepasste Einrichtung haben. </translation>
    </message>
</context>
<context>
    <name>BoosterStatusDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="14"/>
        <source>Booster Status Monitor</source>
        <translation>Boosterstatusmonitor</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="40"/>
        <source>FileWave Booster:</source>
        <translation>FileWave-Booster:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="110"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="150"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="183"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="203"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="257"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="297"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="355"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="120"/>
        <source>Build:</source>
        <translation>Build:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="133"/>
        <source>Server Address:</source>
        <translation>Serveradresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="143"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="233"/>
        <source>127.0.0.1</source>
        <translation>127.0.0.1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="160"/>
        <source>12345</source>
        <translation>12345</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="193"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="537"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="223"/>
        <source>Connecting...</source>
        <translation>Verbindung wird hergestellt...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="250"/>
        <source>files remaining to load</source>
        <translation>noch zu ladende Dateien</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="267"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="277"/>
        <source>free space on root disk</source>
        <translation>freier Speicherplatz auf der Root-Disk</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="284"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="307"/>
        <source>Server Port:</source>
        <translation>Server-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="317"/>
        <source>active connections</source>
        <translation>aktive Verbindungen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="324"/>
        <source>remaining to load</source>
        <translation>noch zu laden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="331"/>
        <source>20443</source>
        <translation>20443</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="338"/>
        <source>data transferred</source>
        <translation>übertragene Daten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="365"/>
        <source>Launched:</source>
        <translation>Gestartet:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="378"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="524"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="388"/>
        <source>files boosted</source>
        <translation>geladene Dateien</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="395"/>
        <source>rejected client requests</source>
        <translation>Abgelehnte Client-Anfragen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="449"/>
        <source>Booster Servers:</source>
        <translation>Booster-Server:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="474"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="484"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="494"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="504"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="514"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="550"/>
        <source>Last Contact:</source>
        <translation>Letzter Kontakt:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="563"/>
        <source>Files:</source>
        <translation>Dateien:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="576"/>
        <source>Size:</source>
        <translation>Größe:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="589"/>
        <source>Free space:</source>
        <translation>Freier Speicherplatz:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="921"/>
        <source>Preferences...</source>
        <translation>Präferenzen...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="50"/>
        <source>%1 - Booster Status Monitor</source>
        <translation>%1 - Boosterstatusmonitor</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="134"/>
        <source>Network Error: Booster is not running or otherwise unavailable on the network</source>
        <translation>Netzwerkfehler: Booster läuft nicht oder ist aus anderen Gründen im Netzwerk nicht erhältlich</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="140"/>
        <source>Password incorrect</source>
        <translation>Passwort falsch</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="159"/>
        <source>Waiting to reconnect...</source>
        <translation>Neue Verbindung gesucht...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="168"/>
        <source>Failed to set preferences for Booster %1:%2</source>
        <translation>Die Einstellungen für Booster %1:%2 sind nicht korrekt</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="180"/>
        <source>Reconnecting...</source>
        <translation>Verbindung wird wiederhergestellt...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="192"/>
        <source>Downloading File: FW%1</source>
        <translation>Datei wird heruntergeladen: FW%1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="195"/>
        <source>Broken Network Connection</source>
        <translation>Netzwerkverbindung unterbrochen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="198"/>
        <source>Waiting for Server/Booster</source>
        <translation>Server/Booster gesucht</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="201"/>
        <source>File ID: %1 not found on Booster/Server</source>
        <translation>Datei-ID: %1 nicht auf Booster/Server erfasst</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="204"/>
        <source>File ID: %1 CRC error</source>
        <translation>Datei-ID: %1 CRC-Fehler</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="207"/>
        <source>Running...</source>
        <translation>Läuft...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="257"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="258"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="259"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="260"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="261"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="263"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="264"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="265"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="266"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="267"/>
        <source>disabled</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="269"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="270"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="271"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="272"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="273"/>
        <source>n.a.</source>
        <translation>k. A.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="297"/>
        <source>Monitor Not Connected</source>
        <translation>Monitor nicht verbunden</translation>
    </message>
</context>
<context>
    <name>Boosters</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="105"/>
        <source>IP or DNS Address</source>
        <translation>IP- oder DNS-Adresse</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="187"/>
        <source>Route server messages via Boosters</source>
        <translation>Server-Nachrichten über Booster weiterleiten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="95"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="180"/>
        <source>Booster 1:</source>
        <translation>Booster 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="121"/>
        <source>Booster 2:</source>
        <translation>Booster 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="153"/>
        <source>Booster 3:</source>
        <translation>Booster 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="88"/>
        <source>Booster 4:</source>
        <translation>Booster 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="160"/>
        <source>Booster 5:</source>
        <translation>Booster 5:</translation>
    </message>
</context>
<context>
    <name>CCommunications</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="14"/>
        <source>Communications</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="249"/>
        <source>Monitor Port:</source>
        <translation>Monitor-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="259"/>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="86"/>
        <source>Server Port:</source>
        <translation>Server-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="96"/>
        <source>Tickle Interval:</source>
        <translation>Anstoßintervall:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="182"/>
        <source>ex. 20015</source>
        <translation>ex. 20015</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="162"/>
        <source>Kiosk Port:</source>
        <translation>Kiosk-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="172"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave-Serveradresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="119"/>
        <source>ex. 20010</source>
        <translation>ex. 20010</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="129"/>
        <source>Client Name:</source>
        <translation>Client-Name:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="142"/>
        <source>ex. 20020</source>
        <translation>ex. 20020</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="217"/>
        <source>Sync computer name</source>
        <translation>Computername synchronisieren</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="224"/>
        <source>Use SSL</source>
        <translation>SSL verwenden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.cpp" line="41"/>
        <source>(no value)</source>
        <translation>(kein Wert)</translation>
    </message>
</context>
<context>
    <name>CFWStatusDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="14"/>
        <source>Client Monitor</source>
        <translation>Client-Monitor</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="78"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="120"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="192"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="226"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="99"/>
        <source>Platform:</source>
        <translation>Plattform:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="322"/>
        <source>FileWave Server:</source>
        <translation>FileWave Server:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="365"/>
        <source>Model Version:</source>
        <translation>Modellversion:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="456"/>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="380"/>
        <source>File:</source>
        <translation>Datei:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="174"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="66"/>
        <source>0.0.0.0</source>
        <translation>0.0.0.0</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="580"/>
        <source>Server Connection:</source>
        <translation>Serververbindung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="531"/>
        <source>Last Successful Connection:</source>
        <translation>Zuletzt aufgebaute Verbindung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="565"/>
        <source>Last Connection Attempt:</source>
        <translation>Letzter Verbindungsversuch:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="642"/>
        <source>Verify</source>
        <translation>Überprüfen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="665"/>
        <source>Client Log</source>
        <translation>Client-Protokoll</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="688"/>
        <source>Preferences...</source>
        <translation>Präferenzen...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="141"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="373"/>
        <source>[disconnected]</source>
        <translation>[getrennt]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="151"/>
        <source>Getting Client Prefs...</source>
        <translation>Client-Einstellungen abrufen...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="303"/>
        <source>Imaging Appliance Monitor</source>
        <translation>Imaging-Geräteüberwachung</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="305"/>
        <source>%1 - Client Monitor</source>
        <translation>%1 - Client-Überwachung</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="344"/>
        <source>(never)</source>
        <translation>(Niemals)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="349"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="352"/>
        <source>Connected (updates found)</source>
        <translation>Verbunden (Aktualisierungen liegen vor)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="352"/>
        <source>Connected (no updates)</source>
        <translation>Verbunden (Aktualisierungen liegen nicht vor)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="356"/>
        <source>(now trying...)</source>
        <translation>(Versuch läuft...)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="358"/>
        <source>Not connected</source>
        <translation>Nicht verbunden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="371"/>
        <source>Can&apos;t connect to client...</source>
        <translation>Verbindung zum Client nicht aufgebaut...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="439"/>
        <source>The verify command has been sent to the client</source>
        <translation>Der Verifizierungsbefehl wurde an den Client gesendet</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="441"/>
        <source>Failed to send the verify message to this client</source>
        <translation>Die Verifizierungsmitteilung konnte nicht an diesen Client gesendet werden</translation>
    </message>
</context>
<context>
    <name>CSimplePasswordDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="20"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="21"/>
        <source>Please enter the password needed to change the 
settings for this client</source>
        <translation>Bitte geben Sie das Kennwort zum Ändern der 
Einstellungen dieses Clients an</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="53"/>
        <source>Password Incorrect!</source>
        <translation>Das Passwort ist falsch!</translation>
    </message>
</context>
<context>
    <name>ConnectDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="14"/>
        <source>Connect to Booster</source>
        <translation>Mit Booster verbinden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="71"/>
        <source>FileWave Booster</source>
        <translation>FileWave Booster</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="81"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="91"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.cpp" line="22"/>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.cpp" line="66"/>
        <source>Error when connecting to Booster:
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectingToBoosterWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidget.cpp" line="13"/>
        <source>Connecting to booster %1...</source>
        <translation>Verbindungsaufbau zu Booster %1...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidget.cpp" line="14"/>
        <source>Connecting to booster %1 (%2)</source>
        <translation>Verbindungsaufbau zu Booster %1 (%2)</translation>
    </message>
</context>
<context>
    <name>ConnectingToBoosterWidgetManager</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="26"/>
        <source>The selected booster has a pending connection.
Please wait for the results before opening a new connection</source>
        <translation>Der ausgewählte Booster hat eine ausstehende Verbindung.
Bitte warten Sie die Ergebnisse ab, bevor Sie eine neue Verbindung öffnen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="43"/>
        <source>Unable to connect to the Booster: %1</source>
        <translation>Verbindung zum Booster nicht möglich: %1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="44"/>
        <source>Unable to connect to the Booster: %1 (%2)</source>
        <translation>Verbindung zum Booster nicht möglich: %1 (%2)</translation>
    </message>
</context>
<context>
    <name>CronWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="20"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="33"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="40"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="90"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="124"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="155"/>
        <source>at</source>
        <translation>um</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="47"/>
        <source>Every month on the</source>
        <translation>Jeden Monat am</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="64"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="97"/>
        <source>skip weekends</source>
        <translation>Wochenenden auslassen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="114"/>
        <source>Every day</source>
        <translation>Jeden Tag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="134"/>
        <source>Every hour</source>
        <translation>Jede Stunde</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="141"/>
        <source>Every week on</source>
        <translation>Jede Woche am</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="148"/>
        <source>One-time</source>
        <translation>Einmalig</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="44"/>
        <source>Monday</source>
        <translation>Montag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="45"/>
        <source>Tuesday</source>
        <translation>Dienstag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="46"/>
        <source>Wednesday</source>
        <translation>Mittwoch</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="47"/>
        <source>Thursday</source>
        <translation>Donnerstag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="48"/>
        <source>Friday</source>
        <translation>Freitag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="49"/>
        <source>Saturday</source>
        <translation>Samstag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="50"/>
        <source>Sunday</source>
        <translation>Sonntag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="53"/>
        <source>first</source>
        <translation>ersten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="54"/>
        <source>second</source>
        <translation>zweiten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="55"/>
        <source>third</source>
        <translation>dritten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="56"/>
        <source>fourth</source>
        <translation>vierten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="57"/>
        <source>last</source>
        <translation>letzten</translation>
    </message>
</context>
<context>
    <name>EmailTemplateWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="25"/>
        <source>[Email body]</source>
        <translation>[Email body]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="32"/>
        <source>[Subject line]</source>
        <translation>[Subject line]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="45"/>
        <source>Subject:</source>
        <translation>Thema:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="64"/>
        <source>Body:</source>
        <translation>Textkörper:</translation>
    </message>
</context>
<context>
    <name>FTabWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="92"/>
        <source>Close Tab</source>
        <translation>Registerkarte schließen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="97"/>
        <source>Close All Tabs</source>
        <translation>Alle Registerkarten schließen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="99"/>
        <source>Close Other Tabs</source>
        <translation>Andere Registerkarten schließen</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="49"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="101"/>
        <source>Accepted formats: PNG, JPG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="121"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="128"/>
        <source>Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.cpp" line="12"/>
        <source>Images (*.png *.jpg *.jpeg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.cpp" line="44"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemListWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Formular</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="35"/>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="42"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="81"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="88"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="102"/>
        <source>Add Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="109"/>
        <source>Remove Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.cpp" line="270"/>
        <source>Maximum number of items reached</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KioskCategoryTreeModel</name>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="39"/>
        <source>Category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="39"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="47"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="235"/>
        <source>New Category</source>
        <translation>Neue Kategorie</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="22"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="40"/>
        <source>Client Log</source>
        <translation>Client-Protokoll</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="109"/>
        <source>Sorry, but getting the log file failed.</source>
        <translation>Der Abruf der Protokolldatei ist leider nicht gelungen.</translation>
    </message>
</context>
<context>
    <name>MAIN</name>
    <message>
        <location filename="../../FileWaveGuiLib/GUITools.cpp" line="101"/>
        <source>Another user is modifying one of the objects involved in this operation.</source>
        <translation>Ein anderer Benutzer ändert gerade ein Objekt, das an diesem Vorgang beteiligt ist.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/GUITools.cpp" line="102"/>
        <source>A database error occurred processing this request; please check the server logs (fwxadmin.log) for more information.</source>
        <translation>Bei der Verarbeitung dieser Anfrage ist ein Datenbankfehler aufgetreten; bitte sehen Sie in den Serverprotokollen (fwxadmin.log) nach.</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="14"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="23"/>
        <source>Debug Level:</source>
        <translation>Debug-Level:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="43"/>
        <source>File Check Interval:</source>
        <translation>Dateiprüfintervall:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="65"/>
        <source>minutes</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="74"/>
        <source>Free Space Margin:</source>
        <translation>Spanne des freien Speicherplatzes:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="81"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="97"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="111"/>
        <source>Confirm Password:</source>
        <translation>Bestätigen Sie das Passwort:</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/passworddlg.ui" line="13"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/passworddlg.ui" line="19"/>
        <source>Please enter the password to control this Booster:</source>
        <translation>Bitte geben Sie das Passwort ein, mit dem Sie diesen Booster steuern können:</translation>
    </message>
</context>
<context>
    <name>PreferencesMain</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="79"/>
        <source>Communications</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="82"/>
        <source>Boosters</source>
        <translation>Booster</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="85"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="94"/>
        <source>Privacy</source>
        <translation>Datenschutz</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="116"/>
        <source>FileWave™ Imaging Appliance Preferences</source>
        <translation>Die Einstellungen des FileWave™-Imaging-Gerätes</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="118"/>
        <source>FileWave™ Client Preferences</source>
        <translation>Einstellungen des FileWave™ Clients</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="345"/>
        <source>Cannot confirm password.  Please re-enter password.</source>
        <translation>Passwort kann nicht bestätigt werden.  Bitte geben Sie das Passwort erneut ein.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="346"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>PrefsEditorDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="14"/>
        <source>Superprefs Editor</source>
        <translation>SuperPrefs-Editor</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="20"/>
        <source>These preferences will be merged with the preferences on target clients.</source>
        <translation>Diese Einstellungen werden mit denen der Zielclients zusammengeführt.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="27"/>
        <source>If you would like a value to remain unchanged, leave it blank.</source>
        <translation>Wenn ein Wert unverändert bleiben soll, lassen Sie ihn offen.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="38"/>
        <source>Communications</source>
        <translation>Kommunikation</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="44"/>
        <source>Kiosk Port:</source>
        <translation>Kiosk-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="173"/>
        <source>Tickle Interval:</source>
        <translation>Anstoßintervall:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="117"/>
        <source>Synchronize Client Name with Computer name</source>
        <translation>Client-Name mit Computername synchronisieren</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="189"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave-Serveradresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="215"/>
        <source>ex. 20020</source>
        <translation>ex. 20020</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="60"/>
        <source>Server Port:</source>
        <translation>Server-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="160"/>
        <source>Monitor Port:</source>
        <translation>Monitor-Port:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="130"/>
        <source>ex. 20010</source>
        <translation>ex. 20010</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="86"/>
        <source>ex. 20015</source>
        <translation>ex. 20015</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="150"/>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="100"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="548"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="597"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="610"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="663"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="233"/>
        <source>Boosters</source>
        <translation>Booster</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="239"/>
        <source>Booster 3:</source>
        <translation>Booster 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="281"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="327"/>
        <source>Booster 5:</source>
        <translation>Booster 5:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="340"/>
        <source>Booster 4:</source>
        <translation>Booster 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="412"/>
        <source>Booster 1:</source>
        <translation>Booster 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="428"/>
        <source>Booster 2:</source>
        <translation>Booster 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="454"/>
        <source>IP or DNS Address:</source>
        <translation>IP- oder DNS-Adresse:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="487"/>
        <source>Route server messages via boosters.</source>
        <translation>Server-Nachrichten über Booster weiterleiten.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="498"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="524"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="531"/>
        <source>minutes</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="538"/>
        <source>Priority:</source>
        <translation>Priorität:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="564"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="574"/>
        <source>Verify Password:</source>
        <translation>Passwort abgleichen:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="633"/>
        <source>File Check Interval:</source>
        <translation>Dateiprüfintervall:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="643"/>
        <source>Free Space Margin:</source>
        <translation>Spanne des freien Speicherplatzes:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="653"/>
        <source>Debug Level:</source>
        <translation>Debug-Level:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="687"/>
        <source>Privacy</source>
        <translation>Datenschutz</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="693"/>
        <source>Personal Data</source>
        <translation>Personenbezogene Daten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="712"/>
        <source>Location refresh interval:</source>
        <translation>Intervall für die Ortsaktualisierung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="719"/>
        <source>Disable personal data collection</source>
        <translation>Erhebung personenbezogener Daten deaktivieren</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Collecting personal data may be disabled at an organization level. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Das Erheben personenbezogener Daten kann auf Organisationsebene deaktiviert werden. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="742"/>
        <source>Remote Sessions</source>
        <translation>Fernsitzungen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="748"/>
        <source>Prompt client for remote control access</source>
        <translation>Aufforderung an den Client für den Fernsteuerungszugriff</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="771"/>
        <source>Managed remote control</source>
        <translation>Verwaltete Fernsteuerung</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="114"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="115"/>
        <source>Never</source>
        <translation>Niemals</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="116"/>
        <source>5 minutes</source>
        <translation>5 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="117"/>
        <source>15 minutes</source>
        <translation>15 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="118"/>
        <source>30 minutes</source>
        <translation>30 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="119"/>
        <source>1 hour</source>
        <translation>1 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="120"/>
        <source>2 hours</source>
        <translation>2 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="121"/>
        <source>6 hours</source>
        <translation>6 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="122"/>
        <source>12 hours</source>
        <translation>12 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="123"/>
        <source>1 day</source>
        <translation>1 Tag</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="173"/>
        <source>Select an existing fwcld.newprefs.plist file or click Cancel to start with a blank one...</source>
        <translation>Wählen Sie eine vorhandene Datei fwcld.newprefs.plist oder klicken Sie auf Abbrechen, wenn Sie mit einer leeren Datei beginnen möchten...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="238"/>
        <source>Password verification failed.</source>
        <translation>Passwortüberprüfung fehlerhaft.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="244"/>
        <source>Select a location to save the Superprefs file;  You&apos;ll add it to a fileset later.</source>
        <translation>Wählen Sie einen Speicherort für die Superprefs-Datei; später nehmen Sie sie in ein Fileset auf.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="360"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="388"/>
        <source>You have unsaved changes.  Are you sure you want to quit?</source>
        <translation>Sie haben ungespeicherte Änderungen.  Wollen Sie wirklich schließen?</translation>
    </message>
</context>
<context>
    <name>Privacy</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="20"/>
        <source>Personal Data</source>
        <translation>Personenbezogene Daten</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="39"/>
        <source>Location refresh interval:</source>
        <translation>Intervall für die Ortsaktualisierung:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="46"/>
        <source>Disable personal data collection</source>
        <translation>Erhebung personenbezogener Daten deaktivieren</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Collecting personal data may be disabled at an organization level. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Das Erheben personenbezogener Daten kann auf Organisationsebene deaktiviert werden. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="66"/>
        <source>Remote Sessions</source>
        <translation>Fernsitzungen</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="72"/>
        <source>Prompt client for remote control access</source>
        <translation>Aufforderung an den Client für den Fernsteuerungszugriff</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="92"/>
        <source>Managed remote control</source>
        <translation>Verwaltete Fernsteuerung</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="15"/>
        <source>Never</source>
        <translation>Niemals</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="16"/>
        <source>5 minutes</source>
        <translation>5 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="17"/>
        <source>15 minutes</source>
        <translation>15 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="18"/>
        <source>30 minutes</source>
        <translation>30 min</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="19"/>
        <source>1 hour</source>
        <translation>1 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="20"/>
        <source>2 hours</source>
        <translation>2 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="21"/>
        <source>6 hours</source>
        <translation>6 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="22"/>
        <source>12 hours</source>
        <translation>12 Std</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="23"/>
        <source>1 day</source>
        <translation>1 Tag</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterconnection.cpp" line="649"/>
        <source>n.a.</source>
        <comment>not applicable</comment>
        <translation>k. A.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="9"/>
        <source>FCM correctly configured.</source>
        <translation>FCM korrekt konfiguriert.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="11"/>
        <source>Project number not set.</source>
        <translation>Projektnummer nicht eingestellt.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="13"/>
        <source>API Key not set.</source>
        <translation>API-Schlüssel nicht festgelegt.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="15"/>
        <source>Invalid server API key or project not correctly configured in Google Dev. Console.</source>
        <translation>Ungültiger Server-API-Schlüssel oder Projekt nicht korrekt in Google Dev konfiguriert. Konsole.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="17"/>
        <source>FCM not configured.</source>
        <translation>FCM nicht konfiguriert.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="60"/>
        <source>Select an Icon</source>
        <comment>KioskResourceHelper</comment>
        <translation>Wählen Sie ein Icon</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="64"/>
        <source>Select an exe, dll, or icon file</source>
        <comment>KioskResourceHelper</comment>
        <translation>Wählen Sie eine exe-, dll- oder Icon-Datei</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="68"/>
        <source>Select an Application or icon file</source>
        <comment>KioskResourceHelper</comment>
        <translation>Wählen Sie eine Anwendung oder eine Icon-Datei</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="71"/>
        <source>Images (</source>
        <comment>KioskResourceHelper</comment>
        <translation>Bilder (</translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="../../FileWaveGuiLib/searchlineedit/searchlineedit.cpp" line="24"/>
        <source>Press enter to search</source>
        <translation>Drücken Sie zum Suchen die Eingabetaste</translation>
    </message>
</context>
<context>
    <name>StyledTreeView</name>
    <message>
        <location filename="../../FileWaveGuiLib/styledviews/styledtreeview.cpp" line="140"/>
        <source>Resize columns</source>
        <translation>Spaltengröße ändern</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/styledviews/styledtreeview.cpp" line="244"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
</context>
<context>
    <name>ViewSearchHeader</name>
    <message>
        <location filename="../../FileWaveGuiLib/searchheader/ViewSearchHeader.cpp" line="95"/>
        <source>Search:</source>
        <translation>Suchen:</translation>
    </message>
</context>
</TS>
