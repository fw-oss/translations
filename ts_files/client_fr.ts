<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>FW::Catalog</name>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="545"/>
        <source>Checking for new model version</source>
        <translation>Vérification de la dernière version du modèle</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="567"/>
        <source>Check for new model in %1 seconds</source>
        <translation>Vérification du modèle dans %1 secondes</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="848"/>
        <source>Checking Files...</source>
        <translation>Vérification des fichiers...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="849"/>
        <source>Verifying Files...</source>
        <translation>Vérification des fichiers...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="868"/>
        <source>Verifying %1 of %2 filesets</source>
        <translation>Vérification du fileset %1 sur %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="934"/>
        <source>Running..</source>
        <translation>En cours d&apos;exécution..</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="936"/>
        <source>Finished Verifying Files...</source>
        <translation>Vérification des fichiers terminée...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1595"/>
        <source>Downloading User Manifest</source>
        <translation>Téléchargement du manifest utilisateur</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1649"/>
        <source>Downloading Smart Filter Manifests</source>
        <translation>Téléchargement des manifests pour les Smart Filters</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1688"/>
        <location filename="../../QtClient/Catalog.cpp" line="1706"/>
        <location filename="../../QtClient/Catalog.cpp" line="2179"/>
        <source>Running</source>
        <translation>En cours d&apos;exécution</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4092"/>
        <source>Waiting for Booster - %1</source>
        <translation>En attente du Booster - %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4111"/>
        <source>%1 (or dependency of) not found on server</source>
        <translation>Le FIleset %1 (ou une de ses dépendances) n&apos;est pas disponible sur le serveur</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4466"/>
        <source>Downloading Fileset Container (%1)</source>
        <translation>Téléchargement du conteneur de Fileset (%1)</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4468"/>
        <source>Processing requirements of %1</source>
        <translation>Analyse des pré-requis de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4577"/>
        <source>No space left on device...Cannot download new Filesets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4589"/>
        <source>Downloading fileset %1 of %2</source>
        <translation>Téléchargement du fileset %1 sur %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5252"/>
        <source>Updating %1 to new version</source>
        <translation>Mise à jour de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5876"/>
        <source>&lt;Fileset name still unknown&gt; ID:%1, revision ID:%2</source>
        <translation>&lt;Fileset inconnu&gt; ID:%1, revision ID: %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="1622"/>
        <source>Downloading Imaging Manifest</source>
        <translation>Téléchargement du manifest Imaging</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4628"/>
        <source>Activating %1</source>
        <translation>Activation de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4638"/>
        <source>Activating fileset %1 of %2</source>
        <translation>Activation du fileset %1 sur %2</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4753"/>
        <source>Installing Software...</source>
        <translation>Installation de logiciel...</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4889"/>
        <source> Installation Failed</source>
        <translation> Installation échouée</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="4896"/>
        <source> Installation Successfully</source>
        <translation> Installation réussie</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5839"/>
        <source>Current Model Number: %1

Filesets in Inventory:

</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5853"/>
        <source>Filesets not meeting requirements:

</source>
        <translation>Filesets ne satisfaisant pas les prérequis:

</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5866"/>
        <source>
Worklist:

</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5951"/>
        <source>This computer will be shut down once its data is erased</source>
        <translation>Cet ordinateur va être éteint une fois ces données effacées</translation>
    </message>
    <message>
        <location filename="../../QtClient/Catalog.cpp" line="5952"/>
        <source>Executing Remote Wipe</source>
        <translation>Exécution de Remote Wipe</translation>
    </message>
</context>
<context>
    <name>FW::FilesetContainer</name>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="563"/>
        <source>Fileset </source>
        <translation>Fileset </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="575"/>
        <source>revision </source>
        <translation>révision </translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="608"/>
        <source>Processing %1</source>
        <translation>Traitement de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="907"/>
        <source>Sending download request to server...</source>
        <translation type="unfinished">Envoi de la requête de téléchargement au serveur...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="913"/>
        <source>Downloading %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1621"/>
        <source>Deleting empty folders of %1</source>
        <translation>Suppression des dossiers vides de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1672"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1796"/>
        <source>Making %1 passive</source>
        <translation>Le Fileset %1 devient passif</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1886"/>
        <source>Deleting %1</source>
        <translation>Suppression de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1952"/>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2015"/>
        <source>Executing %1...</source>
        <translation>Exécution de %1...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="1954"/>
        <source>Executing Files...</source>
        <translation>Exécution des fichiers...</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2094"/>
        <source>Checking Files..</source>
        <translation>Vérification des fichiers..</translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2137"/>
        <source>Activating Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../QtClient/FilesetContainer.cpp" line="2219"/>
        <source>Running..</source>
        <translation>En cours d&apos;exécution..</translation>
    </message>
</context>
<context>
    <name>FW::MainController</name>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="208"/>
        <source>Client is archived.</source>
        <translation>Le client est archivé.</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="213"/>
        <source>Running...</source>
        <translation>En cours d&apos;exécution...</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="219"/>
        <source>Installing %1</source>
        <translation>Installation %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="224"/>
        <location filename="../../QtClient/MainController.cpp" line="344"/>
        <source>Blocked by local restriction</source>
        <translation>Bloqué par une restriction locale</translation>
    </message>
    <message>
        <location filename="../../QtClient/MainController.cpp" line="367"/>
        <source>Connecting to FW Server...</source>
        <translation>Connexion au serveur FW...</translation>
    </message>
</context>
<context>
    <name>FW::ServerConnection</name>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1242"/>
        <source>Building list of files to download...</source>
        <translation>Génération de la liste des fichiers à télécharger...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1261"/>
        <source>Downloading subset for %1</source>
        <translation>Téléchargement d&apos;une partie de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1320"/>
        <location filename="../../QtClient/ServerConnection.cpp" line="1556"/>
        <source>Downloading %1</source>
        <translation>Téléchargement de %1</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1474"/>
        <source>Sending download request to server...</source>
        <translation>Envoi de la requête de téléchargement au serveur...</translation>
    </message>
    <message>
        <location filename="../../QtClient/ServerConnection.cpp" line="1557"/>
        <source>Downloading...</source>
        <translation>Téléchargement...</translation>
    </message>
</context>
<context>
    <name>KioskServerController</name>
    <message>
        <location filename="../../QtClient/kiosk/KioskServerController.cpp" line="377"/>
        <source>Installing Software...</source>
        <translation>Installation de logiciel...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="97"/>
        <source>Fileset </source>
        <translation>Fileset </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="107"/>
        <source>revision </source>
        <translation>révision </translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="127"/>
        <source>checkRequirements</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="128"/>
        <source>downloadFiles</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="129"/>
        <source>activateFiles</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="130"/>
        <source>makeFilesPassive</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="131"/>
        <source>updateFileset</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="132"/>
        <source>deleteFiles</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../QtClient/ActionContainer.cpp" line="133"/>
        <source>updateFilesetDependencies</source>
        <comment>ActionRecord</comment>
        <translation></translation>
    </message>
</context>
</TS>
