<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR" sourcelanguage="en">
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="94"/>
        <source>Version  %1</source>
        <translation>버전 %1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="109"/>
        <source>US Patent # 7,904,900 B2</source>
        <translation>미국 특허 #7,904,900 B2</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/AboutWidget.ui" line="124"/>
        <source>Copyright © FileWave AG %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoosterPreferencesDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="14"/>
        <source>FileWave Booster Preferences</source>
        <translation>FileWave 부스터 기본 설정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="20"/>
        <source>Booster Prefs</source>
        <translation>부스터 기본 설정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="26"/>
        <source>Booster Name:</source>
        <translation>부스터 이름:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="36"/>
        <source>Booster Location:</source>
        <translation>부스터 위치:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="52"/>
        <source>Booster Port:</source>
        <translation>부스터 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="87"/>
        <source>Password:</source>
        <translation>비밀번호:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="110"/>
        <source>Confirmation:</source>
        <translation>확인:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="127"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave 서버 주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="140"/>
        <source>Inventory port:</source>
        <translation>인벤토리 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="183"/>
        <source>Number of Threads:</source>
        <translation>스레드 수:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="194"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="199"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="204"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="209"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="214"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="219"/>
        <source>96</source>
        <translation>96</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="224"/>
        <source>128</source>
        <translation>128</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="232"/>
        <source>Maximum Client Connections:</source>
        <translation>최대 클라이언트 연결 수:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="269"/>
        <source>Debug Level:</source>
        <translation>디버그 수준:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="280"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="285"/>
        <source>99</source>
        <translation>99</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="290"/>
        <source>101</source>
        <translation>101</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="304"/>
        <source>Delete Unused Filesets:</source>
        <translation>사용되지 않는 파일 세트 삭제:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="330"/>
        <source>Fileset Validation Interval:</source>
        <translation>파일 세트 유효성 검사 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="364"/>
        <source>hours</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="379"/>
        <source>Client Download Speed Limit:</source>
        <translation>클라이언트 다운로드 속도 제한:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="413"/>
        <source>Each client will be limited to the specified download rate from this booster.  Uncheck this box to remove the download limit.</source>
        <translation>각 클라이언트는 이 부스터에서 지정된 다운로드 속도로 제한됩니다.  다운로드 제한을 제거하려면 이 상자의 선택을 취소하십시오.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="432"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="444"/>
        <source>Booster Server Prefs</source>
        <translation>부스터 서버 기본 설정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="585"/>
        <source>Server 5:</source>
        <translation>서버 5:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="598"/>
        <source>Server 2:</source>
        <translation>서버 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="542"/>
        <source>IP or DNS Address</source>
        <translation>IP 또는 DNS 주소</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="568"/>
        <source>Server 4:</source>
        <translation>서버 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="522"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="532"/>
        <source>Server 3:</source>
        <translation>서버 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.ui" line="558"/>
        <source>Server 1:</source>
        <translation>서버 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="63"/>
        <source>Booster Error</source>
        <translation>부스터 오류</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="64"/>
        <source>Failed to get a valid connection to Booster %1:%2</source>
        <translation>부스터 %1:%2에 올바르게 연결하지 못했습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="121"/>
        <source>Password verification failed</source>
        <translation>비밀번호 확인 실패</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="127"/>
        <source>FileWave Server Address cannot be empty</source>
        <translation>FileWave 서버 주소를 비워 둘 수 없습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="145"/>
        <source>&quot;%1&quot; for field %2 is an invalid value and was adjusted. Please check.</source>
        <translation>%2 필드에 대한 &quot;%1&quot;은(는) 잘못된 값이며 조정되었습니다. 확인하십시오.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="183"/>
        <source>Illegal Port Used: 20016</source>
        <translation>잘못된 포트 사용: 20016</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="184"/>
        <source>Use port 20015 to connect to the FileWave Server or 20013 to connect to other Boosters. Port 20016 is reserved for the FileWave Admin</source>
        <translation>포트 20015를 사용하여 FileWave 서버에 연결하거나 포트 20013을 사용하여 다른 부스터에 연결합니다. 포트 20016은 FileWave 관리자용으로 예약되어 있습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="211"/>
        <source>Non-standard server ports were chosen.</source>
        <translation>비표준 서버 포트가 선택되었습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterpreferencesdlg.cpp" line="212"/>
        <source>Use port 20015 or 20013 to connect to the FileWave Server or other Boosters. Do you want to save anyway?

You may ignore this warning if you have a customized setup. </source>
        <translation>포트 20015 또는 20013을 사용하여 FileWave 서버나 다른 부스터에 연결합니다. 저장하시겠습니까?

사용자 지정된 설정이 있는 경우 이 경고를 무시할 수 있습니다. </translation>
    </message>
</context>
<context>
    <name>BoosterStatusDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="14"/>
        <source>Booster Status Monitor</source>
        <translation>부스터 상태 모니터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="40"/>
        <source>FileWave Booster:</source>
        <translation>FileWave 부스터:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="110"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="150"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="183"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="203"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="257"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="297"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="355"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="120"/>
        <source>Build:</source>
        <translation>빌드:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="133"/>
        <source>Server Address:</source>
        <translation>서버 주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="143"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="233"/>
        <source>127.0.0.1</source>
        <translation>127.0.0.1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="160"/>
        <source>12345</source>
        <translation>12345</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="193"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="537"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="223"/>
        <source>Connecting...</source>
        <translation>연결 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="250"/>
        <source>files remaining to load</source>
        <translation>로드할 나머지 파일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="267"/>
        <source>Status:</source>
        <translation>상태:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="277"/>
        <source>free space on root disk</source>
        <translation>루트 디스크의 사용 가능한 공간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="284"/>
        <source>Version:</source>
        <translation>버전:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="307"/>
        <source>Server Port:</source>
        <translation>서버 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="317"/>
        <source>active connections</source>
        <translation>활성 연결</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="324"/>
        <source>remaining to load</source>
        <translation>나머지 로드 대상</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="331"/>
        <source>20443</source>
        <translation>20443</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="338"/>
        <source>data transferred</source>
        <translation>데이터가 전송됨</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="365"/>
        <source>Launched:</source>
        <translation>시작됨:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="378"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="524"/>
        <source>Address:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="388"/>
        <source>files boosted</source>
        <translation>파일이 부스팅됨</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="395"/>
        <source>rejected client requests</source>
        <translation>클라이언트 요청이 거부됨</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="449"/>
        <source>Booster Servers:</source>
        <translation>부스터 서버:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="474"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="484"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="494"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="504"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="514"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="550"/>
        <source>Last Contact:</source>
        <translation>마지막 연결:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="563"/>
        <source>Files:</source>
        <translation>파일:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="576"/>
        <source>Size:</source>
        <translation>크기:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="589"/>
        <source>Free space:</source>
        <translation>사용 가능한 공간:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.ui" line="921"/>
        <source>Preferences...</source>
        <translation>기본 설정...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="50"/>
        <source>%1 - Booster Status Monitor</source>
        <translation>%1 - 부스터 상태 모니터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="134"/>
        <source>Network Error: Booster is not running or otherwise unavailable on the network</source>
        <translation>네트워크 오류: 부스터가 실행 중이 아니거나 네트워크에서 사용할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="140"/>
        <source>Password incorrect</source>
        <translation>비밀번호가 잘못되었습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="159"/>
        <source>Waiting to reconnect...</source>
        <translation>다시 연결 대기 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="168"/>
        <source>Failed to set preferences for Booster %1:%2</source>
        <translation>부스터 %1:%2에 대해 기본 설정을 설정하지 못했습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="180"/>
        <source>Reconnecting...</source>
        <translation>다시 연결하는 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="192"/>
        <source>Downloading File: FW%1</source>
        <translation>파일 다운로드 중: FW%1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="195"/>
        <source>Broken Network Connection</source>
        <translation>네트워크 연결 끊김</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="198"/>
        <source>Waiting for Server/Booster</source>
        <translation>서버/부스터 대기 중</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="201"/>
        <source>File ID: %1 not found on Booster/Server</source>
        <translation>파일 ID: 부스터/서버에서 %1을(를) 찾을 수 없음</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="204"/>
        <source>File ID: %1 CRC error</source>
        <translation>파일 ID: %1 CRC 오류</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="207"/>
        <source>Running...</source>
        <translation>실행 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="257"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="258"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="259"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="260"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="261"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="263"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="264"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="265"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="266"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="267"/>
        <source>disabled</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="269"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="270"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="271"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="272"/>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="273"/>
        <source>n.a.</source>
        <translation>n.a.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterstatusdlg.cpp" line="297"/>
        <source>Monitor Not Connected</source>
        <translation>모니터가 연결되지 않음</translation>
    </message>
</context>
<context>
    <name>Boosters</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="14"/>
        <source>Form</source>
        <translation>양식</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="88"/>
        <source>Booster 4:</source>
        <translation>부스터 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="95"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="105"/>
        <source>IP or DNS Address</source>
        <translation>IP 또는 DNS 주소</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="121"/>
        <source>Booster 2:</source>
        <translation>부스터 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="153"/>
        <source>Booster 3:</source>
        <translation>부스터 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="160"/>
        <source>Booster 5:</source>
        <translation>부스터 5:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="180"/>
        <source>Booster 1:</source>
        <translation>부스터 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Boosters.ui" line="187"/>
        <source>Route server messages via Boosters</source>
        <translation>부스터를 통해 서버 메시지 라우팅</translation>
    </message>
</context>
<context>
    <name>CCommunications</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="14"/>
        <source>Communications</source>
        <translation>통신</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="119"/>
        <source>ex. 20010</source>
        <translation>예: 20010</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="142"/>
        <source>ex. 20020</source>
        <translation>예: 20020</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="162"/>
        <source>Kiosk Port:</source>
        <translation>Kiosk 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="259"/>
        <source>seconds</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="249"/>
        <source>Monitor Port:</source>
        <translation>모니터 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="172"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave 서버 주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="86"/>
        <source>Server Port:</source>
        <translation>서버 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="129"/>
        <source>Client Name:</source>
        <translation>클라이언트 이름:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="96"/>
        <source>Tickle Interval:</source>
        <translation>Tickle 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="217"/>
        <source>Sync computer name</source>
        <translation>컴퓨터 이름 동기화</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="224"/>
        <source>Use SSL</source>
        <translation>SSL 사용</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.ui" line="182"/>
        <source>ex. 20015</source>
        <translation>예: 20015</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Communications.cpp" line="41"/>
        <source>(no value)</source>
        <translation>(값 없음)</translation>
    </message>
</context>
<context>
    <name>CFWStatusDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="14"/>
        <source>Client Monitor</source>
        <translation>클라이언트 모니터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="78"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="120"/>
        <source>Address:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="174"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="66"/>
        <source>0.0.0.0</source>
        <translation>0.0.0.0</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="192"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="226"/>
        <source>Version:</source>
        <translation>버전:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="99"/>
        <source>Platform:</source>
        <translation>플랫폼:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="580"/>
        <source>Server Connection:</source>
        <translation>서버 연결:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="365"/>
        <source>Model Version:</source>
        <translation>모델 버전:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="531"/>
        <source>Last Successful Connection:</source>
        <translation>마지막으로 성공한 연결:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="380"/>
        <source>File:</source>
        <translation>파일:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="456"/>
        <source>Status:</source>
        <translation>상태:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="322"/>
        <source>FileWave Server:</source>
        <translation>FileWave 서버:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="565"/>
        <source>Last Connection Attempt:</source>
        <translation>마지막 연결 시도:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="642"/>
        <source>Verify</source>
        <translation>검증</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="665"/>
        <source>Client Log</source>
        <translation>클라이언트 로그</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.ui" line="688"/>
        <source>Preferences...</source>
        <translation>기본 설정...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="141"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="373"/>
        <source>[disconnected]</source>
        <translation>[연결 끊김]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="151"/>
        <source>Getting Client Prefs...</source>
        <translation>클라이언트 기본 설정을 가져오는 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="303"/>
        <source>Imaging Appliance Monitor</source>
        <translation>이미징 어플라이언스 모니터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="305"/>
        <source>%1 - Client Monitor</source>
        <translation>%1 - 클라이언트 모니터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="344"/>
        <source>(never)</source>
        <translation>(안 함)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="349"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="352"/>
        <source>Connected (updates found)</source>
        <translation>연결됨(업데이트 발견)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="352"/>
        <source>Connected (no updates)</source>
        <translation>연결됨(업데이트 없음)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="356"/>
        <source>(now trying...)</source>
        <translation>(지금 시도 중...)</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="358"/>
        <source>Not connected</source>
        <translation>연결되지 않음</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="371"/>
        <source>Can&apos;t connect to client...</source>
        <translation>클라이언트에 연결할 수 없습니다...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="439"/>
        <source>The verify command has been sent to the client</source>
        <translation>확인 명령이 클라이언트에 전송되었습니다</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/FWStatusDlg.cpp" line="441"/>
        <source>Failed to send the verify message to this client</source>
        <translation>이 클라이언트에 확인 메시지를 보내지 못했습니다</translation>
    </message>
</context>
<context>
    <name>CSimplePasswordDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="20"/>
        <source>Password:</source>
        <translation>비밀번호:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="21"/>
        <source>Please enter the password needed to change the 
settings for this client</source>
        <translation>이 클라이언트의 설정을 변경하는 데 필요한 비밀번호를 입력하십시오</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/SimplePasswordDlg.cpp" line="53"/>
        <source>Password Incorrect!</source>
        <translation>비밀번호가 올바르지 않습니다!</translation>
    </message>
</context>
<context>
    <name>ConnectDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="14"/>
        <source>Connect to Booster</source>
        <translation>부스터에 연결</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="71"/>
        <source>FileWave Booster</source>
        <translation>FileWave 부스터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="81"/>
        <source>Address:</source>
        <translation>주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.ui" line="91"/>
        <source>Port:</source>
        <translation>포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.cpp" line="22"/>
        <source>Connect</source>
        <translation>연결</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/connectdlg.cpp" line="66"/>
        <source>Error when connecting to Booster:
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConnectingToBoosterWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidget.cpp" line="13"/>
        <source>Connecting to booster %1...</source>
        <translation>부스터 %1에 연결하는 중...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidget.cpp" line="14"/>
        <source>Connecting to booster %1 (%2)</source>
        <translation>부스터 % 1(%2)에 연결하는 중</translation>
    </message>
</context>
<context>
    <name>ConnectingToBoosterWidgetManager</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="26"/>
        <source>The selected booster has a pending connection.
Please wait for the results before opening a new connection</source>
        <translation>선택한 부스터에 보류 중인 연결이 있습니다.
새 연결을 열기 전에 결과를 기다리십시오</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="43"/>
        <source>Unable to connect to the Booster: %1</source>
        <translation>부스터에 연결할 수 없음: %1</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/ConnectingToBoosterWidgetManager.cpp" line="44"/>
        <source>Unable to connect to the Booster: %1 (%2)</source>
        <translation>부스터에 연결할 수 없음: %1(%2)</translation>
    </message>
</context>
<context>
    <name>CronWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="20"/>
        <source>Form</source>
        <translation>양식</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="33"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="40"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="90"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="124"/>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="155"/>
        <source>at</source>
        <translation>시기</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="47"/>
        <source>Every month on the</source>
        <translation>매월</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="64"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="97"/>
        <source>skip weekends</source>
        <translation>주말 건너뛰기</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="114"/>
        <source>Every day</source>
        <translation>매일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="134"/>
        <source>Every hour</source>
        <translation>매시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="141"/>
        <source>Every week on</source>
        <translation>매주</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.ui" line="148"/>
        <source>One-time</source>
        <translation>한 번</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="44"/>
        <source>Monday</source>
        <translation>월요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="45"/>
        <source>Tuesday</source>
        <translation>화요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="46"/>
        <source>Wednesday</source>
        <translation>수요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="47"/>
        <source>Thursday</source>
        <translation>목요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="48"/>
        <source>Friday</source>
        <translation>금요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="49"/>
        <source>Saturday</source>
        <translation>토요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="50"/>
        <source>Sunday</source>
        <translation>일요일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="53"/>
        <source>first</source>
        <translation>첫 번째</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="54"/>
        <source>second</source>
        <translation>두 번째</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="55"/>
        <source>third</source>
        <translation>세 번째</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="56"/>
        <source>fourth</source>
        <translation>네 번째</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/CronWidget.cpp" line="57"/>
        <source>last</source>
        <translation>마지막</translation>
    </message>
</context>
<context>
    <name>EmailTemplateWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="25"/>
        <source>[Email body]</source>
        <translation>[이메일 본문]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="32"/>
        <source>[Subject line]</source>
        <translation>[제목줄]</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="45"/>
        <source>Subject:</source>
        <translation>제목:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/EmailTemplateWidget.ui" line="64"/>
        <source>Body:</source>
        <translation>본문:</translation>
    </message>
</context>
<context>
    <name>FTabWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="92"/>
        <source>Close Tab</source>
        <translation>탭 닫기</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="97"/>
        <source>Close All Tabs</source>
        <translation>모든 탭 닫기</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/FTabWidget.cpp" line="99"/>
        <source>Close Other Tabs</source>
        <translation>다른 탭 닫기</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished">양식</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="49"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="101"/>
        <source>Accepted formats: PNG, JPG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="121"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.ui" line="128"/>
        <source>Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.cpp" line="12"/>
        <source>Images (*.png *.jpg *.jpeg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ImageSelector.cpp" line="44"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemListWidget</name>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">양식</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="35"/>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="42"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="81"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="88"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="102"/>
        <source>Add Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.ui" line="109"/>
        <source>Remove Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ItemListWidget.cpp" line="270"/>
        <source>Maximum number of items reached</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KioskCategoryTreeModel</name>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="39"/>
        <source>Category</source>
        <translation>범주</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="39"/>
        <source>Description</source>
        <translation>설명</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="47"/>
        <source>All</source>
        <translation>모두</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskCategoryTreeModel.cpp" line="235"/>
        <source>New Category</source>
        <translation>새 카테고리</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="22"/>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="40"/>
        <source>Client Log</source>
        <translation>클라이언트 로그</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/LogViewer.cpp" line="109"/>
        <source>Sorry, but getting the log file failed.</source>
        <translation>죄송합니다. 로그 파일을 가져오지 못했습니다.</translation>
    </message>
</context>
<context>
    <name>MAIN</name>
    <message>
        <location filename="../../FileWaveGuiLib/GUITools.cpp" line="101"/>
        <source>Another user is modifying one of the objects involved in this operation.</source>
        <translation>다른 사용자가 이 작업에 관련된 개체 중 하나를 수정하고 있습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/GUITools.cpp" line="102"/>
        <source>A database error occurred processing this request; please check the server logs (fwxadmin.log) for more information.</source>
        <translation>이 요청을 처리하는 동안 데이터베이스 오류가 발생했습니다. 자세한 내용은 서버 로그(fwxadmin.log)를 확인하십시오.</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="14"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="23"/>
        <source>Debug Level:</source>
        <translation>디버그 수준:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="43"/>
        <source>File Check Interval:</source>
        <translation>파일 확인 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="65"/>
        <source>minutes</source>
        <translation>분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="74"/>
        <source>Free Space Margin:</source>
        <translation>사용 가능한 공간 여유:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="81"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="97"/>
        <source>Password:</source>
        <translation>비밀번호:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Options.ui" line="111"/>
        <source>Confirm Password:</source>
        <translation>비밀번호 확인:</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/passworddlg.ui" line="13"/>
        <source>Password</source>
        <translation>비밀번호</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/passworddlg.ui" line="19"/>
        <source>Please enter the password to control this Booster:</source>
        <translation>이 부스터를 제어하려면 비밀번호를 입력하십시오:</translation>
    </message>
</context>
<context>
    <name>PreferencesMain</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="79"/>
        <source>Communications</source>
        <translation>통신</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="82"/>
        <source>Boosters</source>
        <translation>부스터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="85"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="94"/>
        <source>Privacy</source>
        <translation>개인정보 보호</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="116"/>
        <source>FileWave™ Imaging Appliance Preferences</source>
        <translation>FileWave™ 이미징 어플라이언스 기본 설정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="118"/>
        <source>FileWave™ Client Preferences</source>
        <translation>FileWave™ 클라이언트 기본 설정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="345"/>
        <source>Cannot confirm password.  Please re-enter password.</source>
        <translation>비밀번호를 확인할 수 없습니다.  비밀번호를 다시 입력하십시오.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/PreferencesMain.cpp" line="346"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
</context>
<context>
    <name>PrefsEditorDlg</name>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="14"/>
        <source>Superprefs Editor</source>
        <translation>Superprefs 편집기</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="20"/>
        <source>These preferences will be merged with the preferences on target clients.</source>
        <translation>이 기본 설정은 대상 클라이언트의 기본 설정과 병합됩니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="27"/>
        <source>If you would like a value to remain unchanged, leave it blank.</source>
        <translation>값을 변경하지 않으려면 이 부분을 비워 두십시오.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="38"/>
        <source>Communications</source>
        <translation>통신</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="44"/>
        <source>Kiosk Port:</source>
        <translation>Kiosk 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="173"/>
        <source>Tickle Interval:</source>
        <translation>Tickle 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="117"/>
        <source>Synchronize Client Name with Computer name</source>
        <translation>클라이언트 이름과 컴퓨터 이름 동기화</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="189"/>
        <source>FileWave Server Address:</source>
        <translation>FileWave 서버 주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="215"/>
        <source>ex. 20020</source>
        <translation>예: 20020</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="60"/>
        <source>Server Port:</source>
        <translation>서버 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="160"/>
        <source>Monitor Port:</source>
        <translation>모니터 포트:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="130"/>
        <source>ex. 20010</source>
        <translation>예: 20010</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="86"/>
        <source>ex. 20015</source>
        <translation>예: 20015</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="150"/>
        <source>seconds</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="100"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="548"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="597"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="610"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="663"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="233"/>
        <source>Boosters</source>
        <translation>부스터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="239"/>
        <source>Booster 3:</source>
        <translation>부스터 3:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="281"/>
        <source>Port</source>
        <translation>포트</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="327"/>
        <source>Booster 5:</source>
        <translation>부스터 5:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="340"/>
        <source>Booster 4:</source>
        <translation>부스터 4:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="412"/>
        <source>Booster 1:</source>
        <translation>부스터 1:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="428"/>
        <source>Booster 2:</source>
        <translation>부스터 2:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="454"/>
        <source>IP or DNS Address:</source>
        <translation>IP 또는 DNS 주소:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="487"/>
        <source>Route server messages via boosters.</source>
        <translation>부스터를 통해 서버 메시지 라우팅.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="498"/>
        <source>Options</source>
        <translation>옵션</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="524"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="531"/>
        <source>minutes</source>
        <translation>분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="538"/>
        <source>Priority:</source>
        <translation>우선순위:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="564"/>
        <source>Password:</source>
        <translation>비밀번호:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="574"/>
        <source>Verify Password:</source>
        <translation>비밀번호 확인:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="633"/>
        <source>File Check Interval:</source>
        <translation>파일 확인 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="643"/>
        <source>Free Space Margin:</source>
        <translation>사용 가능한 공간 여유:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="653"/>
        <source>Debug Level:</source>
        <translation>디버그 수준:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="687"/>
        <source>Privacy</source>
        <translation>개인정보 보호</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="693"/>
        <source>Personal Data</source>
        <translation>개인 데이터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="712"/>
        <source>Location refresh interval:</source>
        <translation>위치 새로 고침 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="719"/>
        <source>Disable personal data collection</source>
        <translation>개인 데이터 수집 사용 안 함</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="732"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Collecting personal data may be disabled at an organization level. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;개인 데이터 수집은 조직 수준에서 사용하지 않도록 설정될 수 있습니다. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="742"/>
        <source>Remote Sessions</source>
        <translation>원격 세션</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="748"/>
        <source>Prompt client for remote control access</source>
        <translation>클라이언트에 원격 제어 액세스 요청</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.ui" line="771"/>
        <source>Managed remote control</source>
        <translation>관리형 원격 제어</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="114"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="115"/>
        <source>Never</source>
        <translation>안 함</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="116"/>
        <source>5 minutes</source>
        <translation>5분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="117"/>
        <source>15 minutes</source>
        <translation>15분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="118"/>
        <source>30 minutes</source>
        <translation>30분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="119"/>
        <source>1 hour</source>
        <translation>1시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="120"/>
        <source>2 hours</source>
        <translation>2시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="121"/>
        <source>6 hours</source>
        <translation>6시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="122"/>
        <source>12 hours</source>
        <translation>12시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="123"/>
        <source>1 day</source>
        <translation>1일</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="173"/>
        <source>Select an existing fwcld.newprefs.plist file or click Cancel to start with a blank one...</source>
        <translation>기존 ffcld.newprefs.plist 파일을 선택하거나 빈 파일로 시작하려면 취소를 클릭하십시오...</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="238"/>
        <source>Password verification failed.</source>
        <translation>비밀번호 확인에 실패했습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="244"/>
        <source>Select a location to save the Superprefs file;  You&apos;ll add it to a fileset later.</source>
        <translation>Superprefs 파일을 저장할 위치를 선택하십시오. 나중에 파일 세트에 추가합니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="360"/>
        <location filename="../../FileWaveGuiLib/prefseditordlg.cpp" line="388"/>
        <source>You have unsaved changes.  Are you sure you want to quit?</source>
        <translation>저장하지 않은 변경 사항이 있습니다.  종료하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>Privacy</name>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="14"/>
        <source>Form</source>
        <translation>양식</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="20"/>
        <source>Personal Data</source>
        <translation>개인 데이터</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="39"/>
        <source>Location refresh interval:</source>
        <translation>위치 새로 고침 간격:</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="46"/>
        <source>Disable personal data collection</source>
        <translation>개인 데이터 수집 사용 안 함</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Collecting personal data may be disabled at an organization level. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;개인 데이터 수집은 조직 수준에서 사용하지 않도록 설정될 수 있습니다. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="66"/>
        <source>Remote Sessions</source>
        <translation>원격 세션</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="72"/>
        <source>Prompt client for remote control access</source>
        <translation>클라이언트에 원격 제어 액세스 요청</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.ui" line="92"/>
        <source>Managed remote control</source>
        <translation>관리형 원격 제어</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="15"/>
        <source>Never</source>
        <translation>안 함</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="16"/>
        <source>5 minutes</source>
        <translation>5분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="17"/>
        <source>15 minutes</source>
        <translation>15분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="18"/>
        <source>30 minutes</source>
        <translation>30분</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="19"/>
        <source>1 hour</source>
        <translation>1시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="20"/>
        <source>2 hours</source>
        <translation>2시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="21"/>
        <source>6 hours</source>
        <translation>6시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="22"/>
        <source>12 hours</source>
        <translation>12시간</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/ClientStatusMonitorFiles/Privacy.cpp" line="23"/>
        <source>1 day</source>
        <translation>1일</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../FileWaveGuiLib/BoosterMonitor/boosterconnection.cpp" line="649"/>
        <source>n.a.</source>
        <comment>not applicable</comment>
        <translation>n.a.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="9"/>
        <source>FCM correctly configured.</source>
        <translation>FCM이 올바르게 구성되었습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="11"/>
        <source>Project number not set.</source>
        <translation>프로젝트 번호가 설정되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="13"/>
        <source>API Key not set.</source>
        <translation>API 키가 설정되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="15"/>
        <source>Invalid server API key or project not correctly configured in Google Dev. Console.</source>
        <translation>잘못된 서버 API 키 또는 프로젝트가 Google Dev. Console에서 올바르게 구성되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/Translator.cpp" line="17"/>
        <source>FCM not configured.</source>
        <translation>FCM이 구성되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="60"/>
        <source>Select an Icon</source>
        <comment>KioskResourceHelper</comment>
        <translation>아이콘 선택</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="64"/>
        <source>Select an exe, dll, or icon file</source>
        <comment>KioskResourceHelper</comment>
        <translation>exe, dll 또는 아이콘 파일 선택</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="68"/>
        <source>Select an Application or icon file</source>
        <comment>KioskResourceHelper</comment>
        <translation>애플리케이션 또는 아이콘 파일 선택</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/KioskResourceHelper.cpp" line="71"/>
        <source>Images (</source>
        <comment>KioskResourceHelper</comment>
        <translation>이미지(</translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <location filename="../../FileWaveGuiLib/searchlineedit/searchlineedit.cpp" line="24"/>
        <source>Press enter to search</source>
        <translation>검색하려면 Enter 키를 누르십시오</translation>
    </message>
</context>
<context>
    <name>StyledTreeView</name>
    <message>
        <location filename="../../FileWaveGuiLib/styledviews/styledtreeview.cpp" line="140"/>
        <source>Resize columns</source>
        <translation>열 크기 조정</translation>
    </message>
    <message>
        <location filename="../../FileWaveGuiLib/styledviews/styledtreeview.cpp" line="244"/>
        <source>Copy</source>
        <translation>복사</translation>
    </message>
</context>
<context>
    <name>ViewSearchHeader</name>
    <message>
        <location filename="../../FileWaveGuiLib/searchheader/ViewSearchHeader.cpp" line="95"/>
        <source>Search:</source>
        <translation>검색:</translation>
    </message>
</context>
</TS>
